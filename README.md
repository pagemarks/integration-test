# integration-test

This repository contains data used (and mangled) by Pagemarks' integration tests.

[Pagemarks](https://pagemarks.org) is a free tool that provides Git-based, self-hosted bookmarks.

Unless you are a Pagemark developer, you don't want this repo, you want [this one](https://gitlab.com/barfuin/pagemarks).
